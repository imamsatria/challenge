<?php

class Mahasiswa_model extends CI_model {
    public function getAllMahasiswa()
    {
        $query = $this->db->get('mahasiswa');
        return $query->result_array();
    }

    public function tambahDataMahasiswa()
    {
        $data = [
            "nama" => $this->input->post('nama', true),
            "npm" => $this->input->post('npm', true),
            "tanggal_lahir" => $this->input->post('tanggal_lahir', true),
            "kota" => $this->input->post('kota', true)
        ];

        $this->db->insert('mahasiswa', $data);
    }

    public function hapusDataMahasiswa($npm){
        $this->db->where('npm', $npm);
        $this->db->delete('mahasiswa');
    }

    public function getMahasiswaByNpm($npm){
        return $this->db->get_where('mahasiswa', ['npm' => $npm])->row_array();
    }

    public function ubahDataMahasiswa()
    {
        $data = [
            "nama" => $this->input->post('nama', true),
            "npm" => $this->input->post('npm', true),
            "tanggal_lahir" => $this->input->post('tanggal_lahir', true),
            "kota" => $this->input->post('kota', true)
        ];

        $this->db->where('npm', $this->input->post('npm'));
        $this->db->update('mahasiswa', $data);
    }
}
